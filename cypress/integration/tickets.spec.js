require('cypress-xpath')

describe("Tickets ", () => {
  beforeEach(() => cy.visit("https://ticket-box.s3.eu-central-1.amazonaws.com/index.html"))
  
  it("Fills all the input fields with type Text:", () => {
    const firstName = "Fernando"
    const lastName = "A. M. Leite"
    cy.xpath("//input[@id='first-name']").type(firstName);
    cy.xpath("//input[@id='last-name']").type(lastName);
    cy.xpath("//input[@id='email']").type("fernandoaml@outlook.com");
    cy.xpath("//textarea[@id='requests']").type("vegeterian");
    cy.xpath("//input[@id='signature']").type(firstName + lastName);
  });

  it("select two tickets", () => {
    cy.xpath("//select[@id='ticket-quantity']").select("3")
  });

  it("Select 'VIP' tickt type", () => {
    cy.xpath("//input[@id='vip']").check()
  });

  it("Selects 'Social Media' checkbox", () => {
    cy.get("#social-media").check();
  });

  it("should be able to selects 'friend' and 'publication', after this uncheck 'friend'", () => {
    cy.get("#friend").check();
    cy.get("#publication").check();
    cy.get("#friend").uncheck();


  })

  it("has 'ticketbox' header´s heading ", () => {
    cy.get("header h1").should("contain", "TICKETBOX")
  });

  it('should be alert with a invalid email', () => {
    cy.get("#email")
    .as("email")
    .type("fernandoaml-outlook.com");

    cy.get("#email.invalid")
    .should("exist");

    cy.get("@email")
    .clear()
    .type("fernandoaml@outlook.com");

    cy.get("#email.invalid").should("not.exist");
  });

  it("should be fill and reset all form", () => {
    const firstName = "Fernando";
    const lastName = "A. M. Leite";
    const fullName = `${firstName} ${lastName}`;
    cy.xpath("//input[@id='first-name']").type(firstName);
    cy.xpath("//input[@id='last-name']").type(lastName);
    cy.xpath("//input[@id='email']").type("fernandoaml@outlook.com");
    cy.xpath("//textarea[@id='requests']").type("IPA Beer");
    cy.get("#friend").check();
    cy.xpath("//select[@id='ticket-quantity']").select("3");
    cy.xpath("//input[@id='vip']").check();
    
    cy.get(".agreement p").should(
      "contain", 
      `I, ${fullName}, wish to buy 3 VIP tickets.`
    );
    cy.get('#agree').click();
    cy.xpath("//input[@id='signature']").type(fullName);
    cy.xpath("//button[contains(.,'Confirm Tickets')]")
    .as("submitButton")
    .should("not.be.disabled");

    cy.xpath("//button[@class='reset']").click();
    cy.get("@submitButton").should("be.disabled");
  })

  it("fills mandatory fields usuing support command", () => {
    const customer = {
      firstName: "Fernando",
      lastName: "Leite",
      email: "fernandoaml@outlook.com"
    };

    cy.fillMandatoryFields(customer);

    cy.xpath("//button[contains(.,'Confirm Tickets')]")
    .as("submitButton")
    .should("not.be.disabled");
    cy.get('#agree').uncheck();
    cy.get("@submitButton").should("be.disabled");
  });
});